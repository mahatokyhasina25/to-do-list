using TO_DO.Extension;

var builder = WebApplication.CreateBuilder(args);

builder.Logging.ClearProviders();
builder.Logging.AddConsole();

var environment = builder.Environment;
var configuration = builder.Configuration;

var json = $"appsettings.{environment.EnvironmentName}.json";

builder.Configuration.SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(json);

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.ServiceInjection(configuration); 

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseSwagger();
app.UseSwaggerUI();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
