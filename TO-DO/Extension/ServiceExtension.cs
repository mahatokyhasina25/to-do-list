﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using TO_DO.domaine.Dto;
using TO_DO.domaine.Dto.Request;
using TO_DO.domaine.IServices;
using TO_DO.domaine.IServices.Main;
using TO_DO.domaine.Models;
using TO_DO.infrastructure.Data;
using TO_DO.infrastructure.Services;
using TO_DO.infrastructure.Services.Main;

namespace TO_DO.Extension
{
    public static class ServiceExtension
    {
        public static IServiceCollection ServiceInjection(this IServiceCollection services, IConfiguration configuration)
        {
            InjectBdd(services, configuration);
            InjectService(services, configuration);
            InjectMapper(services);
            Swagger(services);
            return services;
        }
        private static void InjectService(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IUserTaskService, UserTaskService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
        private static void InjectBdd(IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DefaultConnection") ?? throw new InvalidOperationException("Connection string 'DefaultConnection' not found.");
            services.AddDbContext<ApplicationContext>(opt =>
                opt.UseSqlServer(
                    connectionString
                , b => b.MigrationsAssembly("TO-DO"))
            );
        }

        private static void InjectMapper(IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.CreateMap<UserTaskRequestDto, UserTask>();
                mc.CreateMap<UserTask, UserTaskResponseDto>();
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }

        private static void Swagger(IServiceCollection services)
        {

            services.AddSwaggerGen(x =>
            {
                x.MapType<DateTime>(() =>
                {
                    return new OpenApiSchema()
                    {
                        Type = "string",
                        Format = "date-time"
                    };
                });
            });
        }
    }
}
