using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Threading.Tasks;
using TO_DO.domaine.Dto;
using TO_DO.domaine.Dto.Request;
using TO_DO.domaine.IServices;
using TO_DO.Models;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace TO_DO.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUserTaskService _userTaskService;
        public HomeController(ILogger<HomeController> logger, IUserTaskService userTaskService)
        {
            _logger = logger;
            _userTaskService = userTaskService;
        }

        public async Task<IActionResult> Index()
        {
            try
            {
                var list = await _userTaskService.GetAll();
                return View(list);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,ex.Message);
            }
            return View();
        }

        public async Task<IActionResult> Formulaire([FromRoute]int? id)
        {
            try
            {
                if(TempData.TryGetValue("oldDataForm", out var  data))
                {
                    var dataStr = Convert.ToString(data)!;
                    var taskIncorret = JsonConvert.DeserializeObject<UserTaskResponseDto>(dataStr);
                    return View(taskIncorret);
                }
                if (!id.HasValue) return View();
                var task = await _userTaskService.GetById(id.Value);
                return View(task);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
            return View();
        }
        #region Without View
        [HttpPost]
        public async Task<IActionResult> UpSert([FromRoute] int? id, [FromForm] UserTaskRequestDto userTask)
        {
            try
            {
                var data = new UserTaskResponseDto(
                        id.HasValue ? id.Value : 0,
                        userTask.Title,
                        userTask.Description,
                        userTask.IsCompleted,
                        userTask.DueDate
                        );
                TempData.Add("oldDataForm", JsonConvert.SerializeObject(data));
                if (!ModelState.IsValid)
                {
                    return RedirectToAction("Formulaire", new { id = id, errorMessage = "Il y a eu un probleme par rapport au donnes" });
                }
                if (!id.HasValue)
                {
                    await _userTaskService.Create(userTask);
                } else
                {
                    await _userTaskService.Update(id.Value, userTask);
                }
                TempData.Clear();
                return RedirectToAction("");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
            return RedirectToAction("Formulaire", new { id=id, errorMessage = "Il y a eu un probleme" });
        }
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            try
            {
                await _userTaskService.Delete(id);
                return RedirectToAction("");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
            return RedirectToAction("",new { errorMessage = "Il y a eu un probleme" });
        }
        #endregion
    }
}
