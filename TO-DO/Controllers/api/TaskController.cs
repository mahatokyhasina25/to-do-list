﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TO_DO.domaine.Dto;
using TO_DO.domaine.Dto.Request;
using TO_DO.domaine.IServices;

namespace TO_DO.Controllers.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUserTaskService _userTaskService;
        public TaskController(ILogger<HomeController> logger, IUserTaskService userTaskService)
        {
            _logger = logger;
            _userTaskService = userTaskService;
        }

        [HttpGet("GetTasks")]
        [ProducesResponseType(StatusCodes.Status200OK,Type = typeof(List<UserTaskResponseDto>))]
        public async Task<IActionResult> GetTasks()
        {
            ActionResult result=this.BadRequest();
            try
            {
                result = Ok(await _userTaskService.GetAll());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                result = Problem(ex.Message);
            }
            return result;
        }
        [HttpGet("GetTask/{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UserTaskResponseDto))]
        public async Task<IActionResult> GetTask([FromRoute] int id)
        {
            ActionResult result = this.BadRequest();
            if (id <= 0) return result;
            try
            {
                result = Ok(await _userTaskService.GetById(id));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                result = Problem(ex.Message);
            }
            return result;
        }
        [HttpPost("CreateTask")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<IActionResult> CreateTask([FromBody] UserTaskRequestDto userTask)
        {
            ActionResult result = this.BadRequest();
            if (!ModelState.IsValid) return result;
            try
            {
                await _userTaskService.Create(userTask);
                result = Created();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                result = Problem(ex.Message);
            }
            return result;
        }
        [HttpPut("UpdateTask/{id:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> UpdateTask([FromRoute] int id, [FromBody] UserTaskRequestDto userTask)
        {
            ActionResult result = this.BadRequest();
            if (id <= 0) return result;
            if (!ModelState.IsValid) return result;
            try
            {
                await _userTaskService.Update(id,userTask);
                result = NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                result = Problem(ex.Message);
            }
            return result;
        }

        [HttpDelete("DeleteTask/{id:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> DeleteTask([FromRoute] int id)
        {
            ActionResult result = this.BadRequest();
            if (id <= 0) return result;
            try
            {
                await _userTaskService.Delete(id);
                result = NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                result = Problem(ex.Message);
            }
            return result;
        }
    }
}
