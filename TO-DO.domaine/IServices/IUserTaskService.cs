﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TO_DO.domaine.Dto;
using TO_DO.domaine.Dto.Request;

namespace TO_DO.domaine.IServices
{
    public interface IUserTaskService
    {
        Task Create(UserTaskRequestDto userTask);
        Task Delete(int id);
        Task Update(int id,UserTaskRequestDto userTask);
        Task<UserTaskResponseDto> GetById(int id);
        Task<List<UserTaskResponseDto>> GetAll();
    }
}
