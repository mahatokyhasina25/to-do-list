﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TO_DO.domaine.IServices.Main
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<TEnty> GetRepository<TEnty>() where TEnty : class;
        Task<int> Save();
        Task BeginTransaction();
        Task Commit();
        Task RoolBack();
    }
}
