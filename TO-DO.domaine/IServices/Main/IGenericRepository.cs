﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TO_DO.domaine.IServices.Main
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        Task<TEntity> Create(TEntity entity);
        TEntity Update(TEntity entity);
        TEntity Delete(TEntity entity);
        Task<TEntity> DeleteById(int id);
        Task<TEntity> Single(Expression<Func<TEntity, bool>>? filter = null, string includeProperties = "");
        Task<ICollection<TEntity>> GetAll(Expression<Func<TEntity, bool>>? filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null, string includeProperties = "");
        Task<ICollection<TEntity>> GetByPage(int page = 1, int sizePage = 10, Expression<Func<TEntity, bool>>? filter = null, string includeProperties = "");
        Task<bool> Any(Expression<Func<TEntity, bool>>? filter = null);
        Task<int> Count(Expression<Func<TEntity, bool>>? filter = null);
        DbSet<TEntity> GetSet();
    }
}
