﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TO_DO.domaine.Dto
{
    public record UserTaskResponseDto(
            int Id,
            string Title,
            string Description,
            bool IsCompleted,
            DateTime DueDate
    );
}
