﻿using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TO_DO.domaine.IServices.Main;
using TO_DO.infrastructure.Data;

namespace TO_DO.infrastructure.Services.Main
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _context;
        private IDbContextTransaction? _transaction;
        private readonly ILogger<UnitOfWork> _logger;
        private readonly Dictionary<string, dynamic> repositories;
        public UnitOfWork(ApplicationContext context, ILogger<UnitOfWork> logger)
        {
            _context = context;
            _logger = logger;
            repositories = new();
        }


        public IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            string key = typeof(TEntity).FullName!;
            if (!repositories.TryGetValue(key, out var repo))
            {
                var repository = new GenericRepository<TEntity>(_context);
                repositories.Add(key, repository);
                repo = repositories[key];
            }
            return repo;
        }

        public async Task<int> Save()
        {
            try
            {
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error : {ex}");
                throw;
            }
        }

        public async Task BeginTransaction()
        {
            try
            {
                _transaction?.DisposeAsync();
                _transaction = await _context.Database.BeginTransactionAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error : {ex}");
                throw;
            }
        }
        public async Task Commit()
        {
            try
            {
                if (_transaction == null) throw new Exception("transaction not found");
                await _transaction!.CommitAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error : {ex}");
                throw;
            }
        }

        public async Task RoolBack()
        {
            try
            {
                if (_transaction == null) throw new Exception("transaction not found");
                await _transaction!.RollbackAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error : {ex}");
                throw;
            }
        }

        #region dispose process        
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
