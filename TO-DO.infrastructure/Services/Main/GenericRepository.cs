﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TO_DO.domaine.IServices.Main;
using TO_DO.infrastructure.Data;

namespace TO_DO.infrastructure.Services.Main
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly ApplicationContext _context;
        private readonly DbSet<TEntity> _dbSet;
        public GenericRepository(ApplicationContext context)
        {
            this._context = context;
            this._dbSet = context.Set<TEntity>();
        }

        public async Task<TEntity> Create(TEntity entity)
        {
            await this._dbSet.AddAsync(entity);
            return entity;
        }

        public TEntity Delete(TEntity entity)
        {
            this._dbSet.Remove(entity);
            return entity;
        }

        public async Task<TEntity> DeleteById(int id)
        {
            var entity = await this._dbSet.FindAsync(id);
            return this.Delete(entity!);
        }

        public async Task<ICollection<TEntity>> GetAll(Expression<Func<TEntity, bool>>? filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync();
            }
            else
            {
                return await query.ToListAsync();
            }
        }

        public async Task<ICollection<TEntity>> GetByPage(int page = 1, int sizePage = 10, Expression<Func<TEntity, bool>>? filter = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;
            var skip = sizePage * (page - 1);
            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return await query.Take(sizePage).Skip(skip).ToListAsync();
        }

        public async Task<TEntity> Single(Expression<Func<TEntity, bool>>? filter = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            if (filter != null)
            {
                return await query.FirstAsync(filter);
            }
            return await query.FirstAsync();
        }

        public async Task<bool> Any(Expression<Func<TEntity, bool>>? filter = null)
        {
            IQueryable<TEntity> query = _dbSet;
            if (filter != null)
            {
                return await query.AnyAsync(filter);
            }
            return await query.AnyAsync();
        }

        public TEntity Update(TEntity entity)
        {
            this._dbSet.Update(entity);
            return entity;
        }

        public async Task<int> Count(Expression<Func<TEntity, bool>>? filter = null)
        {
            IQueryable<TEntity> query = _dbSet;
            if (filter != null)
            {
                return await query.CountAsync(filter);
            }
            return await query.CountAsync();
        }

        public DbSet<TEntity> GetSet() { return _dbSet; }
    }
}
