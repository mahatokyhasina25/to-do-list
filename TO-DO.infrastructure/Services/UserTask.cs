﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TO_DO.domaine.Dto;
using TO_DO.domaine.Dto.Request;
using TO_DO.domaine.IServices;
using TO_DO.domaine.IServices.Main;
using TO_DO.domaine.Models;

namespace TO_DO.infrastructure.Services
{
    public class UserTaskService : IUserTaskService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UserTaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task Create(UserTaskRequestDto userTask)
        {
            CheckDto(userTask);
            var repo = _unitOfWork.GetRepository<UserTask>();
            var entity= _mapper.Map<UserTask>(userTask);
            await repo.Create(entity);
            await _unitOfWork.Save();
        }

        public async Task Delete(int id)
        {
            await Exist(id);
            var repo = _unitOfWork.GetRepository<UserTask>();
            await repo.DeleteById(id);
            await _unitOfWork.Save();
        }

        public async Task<List<UserTaskResponseDto>> GetAll()
        {
            var repo = _unitOfWork.GetRepository<UserTask>();
            var entities = await repo.GetAll();
            var dtos = _mapper.Map<List<UserTaskResponseDto>>(entities);
            return dtos;
        }

        public async Task<UserTaskResponseDto> GetById(int id)
        {
            await Exist(id);
            var repo = _unitOfWork.GetRepository<UserTask>();
            var entity = await repo.Single(filter: x => x.Id == id);
            var dto = _mapper.Map<UserTaskResponseDto>(entity); 
            return dto;
        }

        public async Task Update(int id,UserTaskRequestDto userTask)
        {
            await Exist(id);
            CheckDto(userTask);
            var repo = _unitOfWork.GetRepository<UserTask>();
            var entity = _mapper.Map<UserTask>(userTask);
            entity.Id = id;
            repo.Update(entity);
            await _unitOfWork.Save();
        }

        private void CheckDto(UserTaskRequestDto userTask)
        {
            if(string.IsNullOrEmpty(userTask.Title)) throw new InvalidOperationException("Title of task not correctly");
            if (string.IsNullOrEmpty(userTask.Description)) throw new InvalidOperationException("Title of task not correctly");
            if (userTask.DueDate<DateTime.Now) throw new InvalidOperationException("Time of task not correctly");
        }
        private async Task Exist(int id)
        {
            var repo = _unitOfWork.GetRepository<UserTask>();
            var exist = await repo.Any(x => x.Id == id);
            if (!exist) throw new InvalidOperationException("Task not found");
        }
    }
}
