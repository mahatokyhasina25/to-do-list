﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TO_DO.domaine.Models;

namespace TO_DO.infrastructure.Data.TypeConfiguration
{
    public class UserTaskConfiguration : IEntityTypeConfiguration<UserTask>
    {
        public void Configure(EntityTypeBuilder<UserTask> entity)
        {
            entity.ToTable(nameof(UserTask));
            entity.HasKey(x => x.Id);
            entity.Property(x => x.Id)
                .ValueGeneratedOnAdd();
            entity.Property(x => x.Title)
                .IsRequired()
                .HasMaxLength(50);
            entity.Property(x => x.Description)
                .IsRequired();
            entity
                .Property(x => x.DueDate)
                .HasDefaultValue(DateTime.UtcNow.AddDays(1))
                .IsRequired();
        }
    }
}
